<?
$config=array(
  "name" => "Языки сайта",
  "menu_icon" => "icon-globe",
  "status" => "system",
  "windows" => array(
                      "create" => array("width" => 500,"height" => 300),
                      "edit" => array("width" => 600,"height" => 300),
                      "list" => array("width" => 600,"height" => 500),
                    ),
  "right" => array("admin","#GRANTED"),
  "main_table" => "lt_languages",
  "list" => array(
    "name" => array("isLink" => true),
    "url" => array("align" => "center"),
    "is_default" => array("align" => "center"),
    "orders" => array("align" => "center"),
  ),
  "select" => array(
     "default_orders" => array(
                           array("orders" => "ASC"),
                           array("name" => "ASC"),
                         ),
     "default" => array(
        "id_lt_languages" => array(
               "desc" => "Язык",
               "type" => "select_from_table",
               "table" => "lt_languages",
               "key_field" => "id_lt_languages",
               "fields" => array("name"),
               "show_field" => "%1",
               "condition" => "",
               "order" => array ("name" => "ASC"),
               "use_empty" => true,
             ),

     ),
  ),
  "downlink_tables" => array(
    "lt_pages" => array(
                      "key_field" => "id_lt_languages",
                      "name" => "name",
                      "component" => "pages"
                    ),
  ),
);

$actions=array(
  "create" => array(

    "before_code" => "",

  ),
);



?>