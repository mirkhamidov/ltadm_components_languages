<?
$items=array(
  "name" => array(
         "desc" => "Название языка",
         "type" => "text",
         "maxlength" => "255",
         "size" => "30",
         "select_on_edit" => true,
         "js_validation" => array(
           "js_not_empty" => "Поле должно быть не пустым!",
         ),
       ),

  "url" => array(
         "desc" => "Идентификатор языка",
         "type" => "text",
         "maxlength" => "50",
         "size" => "10",
         "select_on_edit" => true,
         "js_validation" => array(
           "js_not_empty" => "Поле должно быть не пустым!",
           "js_match" => array (
             "pattern" => "^[A-Za-z0-9_\-]+$",
             "flags" => "g",
             "error" => "Только латинские символы, цифры и '_','-'!",
           ),
         ),
       ),

  "is_default" => array(
         "desc" => "По умолчанию",
         "type" => "radio",
         "values" => array ("yes" => "Да", "no" => "Нет"),
         "default_value" => "no",
         "unique" => array("clear_all_to_default","set_for_this"),
       ),


  "orders" => array(
         "desc" => "Порядок вывода в списках",
         "type" => "order",
         "js_validation" => array(
           "js_not_empty" => "Поле должно быть не пустым!",
           "js_match" => array (
             "pattern" => "^[\d]+$",
             "flags" => "g",
             "error" => "Только цифры! От 0 до 99999!",
           ),
         ),
       ),
    "image" => array(
         "desc" => "Иконка языка",
         "type" => "file",
         "select_on_edit" => true,
       ),
);
?>